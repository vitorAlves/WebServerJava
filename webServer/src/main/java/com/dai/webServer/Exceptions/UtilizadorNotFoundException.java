package com.dai.webServer.Exceptions;

public class UtilizadorNotFoundException extends RuntimeException {

	public UtilizadorNotFoundException(String exception) {
		super(exception);
	}
}
